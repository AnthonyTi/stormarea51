﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundController : MonoBehaviour
{
    public GameObject SceneController;
    public float speed;
    public GameObject[] conveyor;
    public float goBuck = 12.8f;
    public bool isGround = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!SceneController.GetComponent<SceneController>().pause &&
            SceneController.GetComponent<SceneController>().play &&
            SceneController.GetComponent<SceneController>().alive)
        {
            foreach (GameObject conv in conveyor)
            {
                if (isGround)
                {
                    speed = SceneController.GetComponent<SceneController>().speed;
                }
                conv.transform.position += Vector3.left * speed * Time.deltaTime;
                if (conv.transform.position.x < (goBuck * -1))
                {
                    conv.transform.position = new Vector3(conv.transform.position.x + (2 * goBuck), conv.transform.position.y);
                }

            }
        }
    }
}
