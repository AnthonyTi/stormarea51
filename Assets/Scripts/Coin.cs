﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject SceneController;
    public float speed;
    public bool isBonus;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        if (!SceneController.GetComponent<SceneController>().pause &&
            SceneController.GetComponent<SceneController>().play &&
            SceneController.GetComponent<SceneController>().alive)
        {
            speed = SceneController.GetComponent<SceneController>().speed;
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (transform.position.x < -6.0f)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!isBonus)
            {
                SceneController.GetComponent<SceneController>().countCoin++;
            }
            else
            {
                SceneController.GetComponent<SceneController>().coinUFO++;
            }
            Destroy(this.gameObject);
        }

    }
}
