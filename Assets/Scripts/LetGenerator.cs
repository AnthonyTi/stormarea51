﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetGenerator : MonoBehaviour
{
    public GameObject SceneController;
    public GameObject[] let;
    public GameObject coins;
    public GameObject[] bonus; 
    public float[] heightLine;
    public float timer;
    float t = 3.0f;

    void Spawn()
    {
        int row = Random.Range(0, 7);
        int gap = Random.Range(0, 6);
        int idBonus = Random.Range(0, 100);
        if (row == 1 || row == 3 || row == 5)   //001 011 101
        {
            LetInstantiate(heightLine[0], 6);
        }
        if (row == 2 || row == 3 || row == 6)   //010 011 110
        {
            LetInstantiate(heightLine[1], 7);
        }
        if (row == 4 || row == 5 || row == 6)   //100 101 110
        {
            LetInstantiate(heightLine[2], 8);
        }
        if(row == 0)                            //000
        {
            if (gap == 0 || gap == 1)
            {
                CoinInstantiate(heightLine[0], 6);
            }
            if (gap == 2 || gap == 3)
            {
                CoinInstantiate(heightLine[1], 7);
            }
            if (gap == 4 || gap == 5)
            {
                CoinInstantiate(heightLine[2], 8);
            }
        }
        if (row == 1)                           //001
        {
            if(gap <= 2)
            {
                CoinInstantiate(heightLine[1], 7);
            }
            if (gap > 2)
            {
                CoinInstantiate(heightLine[2], 8);
            }
        }
        if (row == 2)                           //010
        {
            if (gap <= 2)
            {
                CoinInstantiate(heightLine[0], 6);
            }
            if (gap > 2)
            {
                CoinInstantiate(heightLine[2], 8);
            }
        }
        if (row == 3)                           //011
        {
            CoinInstantiate(heightLine[2], 8);
        }
        if (row == 4)                           //100
        {
            if (gap <= 2)
            {
                CoinInstantiate(heightLine[0], 6);
            }
            if (gap > 2)
            {
                CoinInstantiate(heightLine[1], 7);
            }
        }
        if (row == 5)                           //101
        {
            CoinInstantiate(heightLine[1], 7);
        }
        if (row == 6)                           //110
        {
            CoinInstantiate(heightLine[0], 6);
        }
        if (idBonus < bonus.Length)
        {
            if (gap <= 2)
            {
                BonusInstantiate(idBonus, -1.46f, 7);
            }
            if (gap > 2)
            {
                BonusInstantiate(idBonus, -0.42f, 6);
            }
            Debug.Log("spawn бонуса №" + idBonus);
        }
    }

    void LetInstantiate(float line, int layer)
    {
        int i = Random.Range(0, let.Length);
        GameObject _let;
        Vector3 pos_inst = new Vector3(this.transform.position.x, line);
        _let = Instantiate(let[i], pos_inst, Quaternion.identity) as GameObject;
        _let.GetComponent<Let>().SceneController = SceneController;
        _let.GetComponent<SpriteRenderer>().sortingOrder = layer;                     // слой рендера
    }

    void CoinInstantiate(float line, int layer)
    {
        GameObject _coin;
        for (int j = -1; j <= 1; j++)
        {
            Vector3 pos_inst = new Vector3(this.transform.position.x + j * 0.75f, line);
            _coin = Instantiate(coins, pos_inst, Quaternion.identity) as GameObject;
            _coin.GetComponent<Coin>().SceneController = SceneController;
            _coin.GetComponent<SpriteRenderer>().sortingOrder = layer;                     // слой рендера
        }
    }

    void BonusInstantiate(int idBonus, float line, int layer)
    {
        GameObject _Bonus;
        Vector3 pos_inst = new Vector3(this.transform.position.x + 2.0f, line);
        _Bonus = Instantiate(bonus[idBonus], pos_inst, Quaternion.identity) as GameObject;
        _Bonus.GetComponent<Coin>().SceneController = SceneController;
        _Bonus.GetComponent<SpriteRenderer>().sortingOrder = layer;                     // слой рендера
    }

    void Update()
    {
        if (!SceneController.GetComponent<SceneController>().pause &&
            SceneController.GetComponent<SceneController>().play &&
            SceneController.GetComponent<SceneController>().alive)
        {
            if (t <= 0)
            {
                t = timer;
                Spawn();
            }
            else
            {
                t -= Time.deltaTime;
            }
        }
    }
}
