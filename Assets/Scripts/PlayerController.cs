﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject SceneController;
    public GameObject target;
    public float speedGoToTarget;
    public float[] deltaLine;
    float posX;

    void Start()
    {
        posX = this.transform.position.x;
    }

    void Update()
    {
        if (!SceneController.GetComponent<SceneController>().pause &&
            SceneController.GetComponent<SceneController>().play &&
            SceneController.GetComponent<SceneController>().alive)
        {
            Vector3 point = target.transform.position;
            point.x = posX;
            this.transform.position = Vector3.MoveTowards(this.transform.position, point, speedGoToTarget * Time.deltaTime);
            float pointY = this.transform.position.y;
            if (pointY >= deltaLine[0])
            {
                this.GetComponent<SpriteRenderer>().sortingOrder = 6;
            }
            if (pointY < deltaLine[0] && pointY >= deltaLine[1])
            {
                this.GetComponent<SpriteRenderer>().sortingOrder = 7;
            }
            if (pointY < deltaLine[1])
            {
                this.GetComponent<SpriteRenderer>().sortingOrder = 8;
            }
        }
    }
}
