﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    public bool play = false;
    public bool pause = false;
    public bool alive = true;
    public float speed = 5.0f;
    public float meters = 0.0f;
    public int Coin;
    public int UFO;
    public float record;
    public int countCoin;
    public int coinUFO;
    public Text textCoin;
    public Text textMeters;
    public int countDeath = 0;
    public GameObject playWindow;
    public GameObject deathWindow;

    // Start is called before the first frame update
    void Start()
    {
        UFO = PlayerPrefs.GetInt("UFO");
    }

    // Update is called once per frame
    void Update()
    {
        if (!pause && play && alive)
        {
            textCoin.text = countCoin.ToString();
            meters += speed * Time.deltaTime;
            textMeters.text = meters.ToString();
        }
    }

    // Триггер пааузы
    public void Pause()
    {
        if (pause)
        {
            pause = false;
        }
        else
        {
            pause = true;
        }
    }

    // Триггер запуска игры
    public void Play(bool _play)
    {
        play = _play;
        if (_play)
        {
            pause = false;
            alive = true;
            countCoin = 0;
            meters = 0;
            coinUFO = 0;
            countDeath = 0;
        }
    }

    // Триггер смерти }:)
    public void Death()
    {
        if (alive)
        {
            alive = false;
            playWindow.SetActive(false);
            deathWindow.SetActive(true);
        }
        else
        {
            alive = true;
        }
    }


    // Сброс игры
    public void RePlay()
    {
        countCoin = 0;
        meters = 0;
        coinUFO = 0;
        countDeath = 0;
        Clean();
    }

    // Отчистка
    public void Clean()
    {
        GameObject[] letList = GameObject.FindGameObjectsWithTag("Let");
        GameObject[] coinList = GameObject.FindGameObjectsWithTag("Coin");
        GameObject[] bonusList = GameObject.FindGameObjectsWithTag("Bonus");
        foreach (GameObject delete in letList)
        {
            Destroy(delete);
        }
        foreach (GameObject delete in coinList)
        {
            Destroy(delete);
        }
        foreach (GameObject delete in bonusList)
        {
            Destroy(delete);
        }
    }

    // Сохранение результатов
    public void Save()
    {
        if (!(PlayerPrefs.HasKey("Coins") || PlayerPrefs.HasKey("Record") || PlayerPrefs.HasKey("UFO")))
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("Данные удалены");
        }
        countCoin += PlayerPrefs.GetInt("Coins");
        PlayerPrefs.SetInt("Coins", countCoin);
        if (meters > PlayerPrefs.GetFloat("Record"))
        {
            PlayerPrefs.SetFloat("Record", meters);
        }
        coinUFO += PlayerPrefs.GetInt("UFO");
        PlayerPrefs.SetInt("UFO", coinUFO);
        PlayerPrefs.Save();
    }
}
