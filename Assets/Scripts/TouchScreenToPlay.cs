﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchScreenToPlay : MonoBehaviour, IDragHandler, IBeginDragHandler
{
    public Camera mainCamera;
    public GameObject SceneController;
    public GameObject playerTarget;
    public float maxY;
    public float minY;
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (maxY <= minY)
        {
            Debug.LogWarning("maxY должен быть больше minY!!!");
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        // Проверка паузы
        if (!SceneController.GetComponent<SceneController>().pause &&
            SceneController.GetComponent<SceneController>().play &&
            SceneController.GetComponent<SceneController>().alive)
        {
            Vector3 point = mainCamera.ScreenToWorldPoint(eventData.position);
            point.z = 0;
            if (point.y > maxY)
            {
                point.y = maxY;
            }
            if (point.y < minY)
            {
                point.y = minY;
            }
            playerTarget.transform.position = point;
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
