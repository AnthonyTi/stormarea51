﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathPanelController : MonoBehaviour
{
    public GameObject SceneController;
    public GameObject playWindow;
    public GameObject deathWindow;
    public GameObject gameOverWindow;
    public float timer;
    float _timer;
    public Image circleBar;
    public Text deathPanelCountUFO;
    //uint countDeath = 1;
    public Text cost;

    private void OnEnable()
    {
        _timer = timer;
        circleBar.fillAmount = 1;
        //deathPanelCountUFO.text = SceneController.GetComponent<SceneController>().UFO.ToString();
        deathPanelCountUFO.text = PlayerPrefs.GetInt("UFO").ToString();
        if(SceneController.GetComponent<SceneController>().countDeath == 0)
        {
            cost.text = "1";
        }
        else
        {
            int _cost = SceneController.GetComponent<SceneController>().countDeath * 2;
            cost.text = _cost.ToString();
        }
    }

    void Update()
    {
        if (_timer > 0)
        {
            _timer -= Time.deltaTime;
            circleBar.fillAmount = _timer / timer;
        }
        else
        {
            deathWindow.SetActive(false);
            gameOverWindow.SetActive(true);
        }
    }

    public void SellUFO()
    {
        if (SceneController.GetComponent<SceneController>().countDeath == 0)
        {
            if(PlayerPrefs.GetInt("UFO") > 0)
            {
                SceneController.GetComponent<SceneController>().Clean();
                SceneController.GetComponent<SceneController>().UFO--;
                SceneController.GetComponent<SceneController>().countDeath++;
                SceneController.GetComponent<SceneController>().alive = true;
                playWindow.SetActive(true);
                deathWindow.SetActive(false);
            }
        }
        else if (SceneController.GetComponent<SceneController>().countDeath * 2 <= SceneController.GetComponent<SceneController>().UFO)
        {
            SceneController.GetComponent<SceneController>().Clean();
            SceneController.GetComponent<SceneController>().UFO -= SceneController.GetComponent<SceneController>().countDeath * 2;
            SceneController.GetComponent<SceneController>().countDeath++;
            SceneController.GetComponent<SceneController>().alive = true;
            playWindow.SetActive(true);
            deathWindow.SetActive(false);
        }
        else
        {
            Debug.Log("Купи тарелочки!");
        }
    }

    public void WatchADS()
    {
        Debug.LogWarning("ДОПИСАТЬ ПРОСМОТР РЕКЛАМЫ!!!");
    }
}
