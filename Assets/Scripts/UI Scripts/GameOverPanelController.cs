﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanelController : MonoBehaviour
{
    public GameObject SceneController;
    public Text coinsText;
    public Text distanceText;
    public Text recordText;

    void Start()
    {
        
    }

    private void OnEnable()
    {
        coinsText.text = SceneController.GetComponent<SceneController>().countCoin.ToString();
        distanceText.text = SceneController.GetComponent<SceneController>().meters.ToString() + "m";
        recordText.text = PlayerPrefs.GetFloat("Record").ToString() + "m";
        SceneController.GetComponent<SceneController>().Save();
        SceneController.GetComponent<SceneController>().Clean();
    }

    void Update()
    {
        
    }
}
