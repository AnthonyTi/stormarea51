﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartWindowPanelController : MonoBehaviour
{
    public GameObject SceneController;
    public Text recordText;
    public Text coinsText;
    public Text UFOText;
    public Text timerToADS;

    void Start()
    {
        
    }

    public void OnEnable()
    {
        recordText.text = PlayerPrefs.GetFloat("Record").ToString() + "m";
        coinsText.text = PlayerPrefs.GetInt("Coins").ToString();
        UFOText.text = PlayerPrefs.GetInt("UFO").ToString();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
